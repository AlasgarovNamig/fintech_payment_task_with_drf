from rest_framework import serializers
from account.models import Account, Payment, SendPayment, ApprovePayment
from decimal import Decimal

class AccountSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Account
        fields = '__all__'

    def validate_balance(self,values):
        if values<0:
            raise serializers.ValidationError("Balance can't be negative !!! ")  
        return values      


class ApprovePeymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApprovePayment
        fields = ['account', 'amount','direction','from_account']
               
    def validate(self,data):
        if data['account'].currency != data['from_account'].currency:
            raise serializers.ValidationError("Only payments within the same currency are supported ( no exchanges )   ")
        return data  


class SendPeymentSerializer(serializers.ModelSerializer):

    class Meta:
        model =  SendPayment
        fields = ['account', 'amount','direction','to_account']
               
    def validate(self,data):
        if data['account'].balance<Decimal(data['amount']):
            raise serializers.ValidationError("You don't have enough funds in your balance !!!")
        elif data['account'].currency.lower() != data['to_account'].currency.lower():
            raise serializers.ValidationError("Only payments within the same currency are supported ( no exchanges ) ")
        return data  
   


class PeymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment 
        fields = '__all__'

                