from django.urls import path
from account.api import views as api_views


urlpatterns = [
    path('accounts/', api_views.AccountListAndCreatAPIView.as_view(), name='account-list'),
    path('payments/', api_views.PaymentListAndCreateAPIViews.as_view(), name='payment-list'),

]