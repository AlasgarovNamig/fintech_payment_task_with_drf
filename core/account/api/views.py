from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import get_object_or_404,GenericAPIView
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from account.models import Account, Payment,ApprovePayment,SendPayment
from account.api.serializers import AccountSerializer , SendPeymentSerializer, ApprovePeymentSerializer, PeymentSerializer
from decimal import Decimal
from django.db import transaction


class AccountListAndCreatAPIView(ListModelMixin,CreateModelMixin,GenericAPIView):
    
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)   
  

class PaymentListAndCreateAPIViews(APIView):

    def get_object(self,pk):
        account_instance = get_object_or_404(Account,pk=pk)
        return account_instance


    def get(self,request):
        approve_payment_obj = ApprovePayment.objects.all()
        send_payment_obj = SendPayment.objects.all()
        serializer_class_approve_payment = ApprovePeymentSerializer(approve_payment_obj, many = True, context = {'request':request})
        serializer_class_send_payment = SendPeymentSerializer(send_payment_obj, many = True, context = {'request':request})
        return Response({
            " APPROVE_PAYMENTS ":serializer_class_approve_payment.data,
            " SEND_PAYMENTS ":serializer_class_send_payment.data
        })    

    def post(self, request):
        
        if request.data['direction']  == 'outgoing' :
            sendSerializer = SendPeymentSerializer(data=request.data)
            if sendSerializer.is_valid():
                account_obj = self.get_object(request.data['account'])   
                account_obj.balance -= Decimal(request.data['amount'])
                try:
                    with transaction.atomic():
                        account_obj.save()
                        sendSerializer.save()
                except Exception as e:
                    print(e)
                return Response(sendSerializer.data, status=status.HTTP_201_CREATED)
            return Response(sendSerializer.errors, status=status.HTTP_400_BAD_REQUEST)     
        elif request.data['direction']  == 'incoming' :
            approveSerializer = ApprovePeymentSerializer(data=request.data) 
            if approveSerializer.is_valid():
                account_obj= self.get_object(request.data['account'])
                account_obj.balance += Decimal(request.data['amount'])
                try:
                    with transaction.atomic():
                        account_obj.save()
                        approveSerializer.save()
                except Exception as e:
                    print(e)
                return Response(approveSerializer.data, status=status.HTTP_201_CREATED) 
            return Response(status=status.HTTP_400_BAD_REQUEST) 
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST) 

       