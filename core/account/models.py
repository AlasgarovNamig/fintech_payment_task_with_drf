from django.db import models
from decimal import Decimal
from core.settings import DIRECTIONS
from django.utils.translation import gettext_lazy as _


class Account(models.Model): 

    id = models.CharField(primary_key=True, max_length=50)
    balance = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal(0.00)) 
    currency = models.CharField(null=False, blank=False,max_length=10)

    def __str__(self):
        return self.id

    class Meta:
        db_table = 'account' 


class Payment(models.Model):

    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account')
    amount = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal(0.00)) 
    direction = models.CharField(_("direction"),choices=DIRECTIONS, max_length=10)


class ApprovePayment(Payment):

    from_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='from_account')


class SendPayment(Payment):

    to_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='to_account')  


# class Payment(models.Model):#makale
#     DIRECTIONS =[
#         ('outgoing','OUT_GOING'),
#         ('incomig','IN_COMING')
#     ] 

#     from_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='from_account')
#     to_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='to_account')
#     amount = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal(0.00)) 
#     direction = models.CharField(_("direction"),choices=DIRECTIONS, max_length=10)

#     # class Meta:
#     #     db_table = 'account_payment'      
