from rest_framework.test import APITestCase
from .models import Account, Payment, ApprovePayment, SendPayment

class TestAccount(APITestCase):
    url = "/api/accounts/"
    def setUp(self):
        Account.objects.create(id="nikki001",balance=100.00,currency = "USD")
    def test_get(self):
        #process
        response = self.client.get(self.url)  
        result = response.json()
        #assert
        self.assertEqual(response.status_code, 200) 
        self.assertIsInstance(result, list)
        self.assertEqual(result[0]["id"], "nikki001")
        self.assertEqual(result[0]["balance"], "100.00")
        self.assertEqual(result[0]["currency"], "USD")
    def test_post(self):
        # definition
        data = {
                "id":"ziko002",
                "balance":"1.00",
                "currency":"USD"
                }
        # process
        response = self.client.post(self.url, data=data)
        result = response.json()
        # assert
        self.assertEqual(response.status_code, 201)
        self.assertEqual(result["id"], "ziko002")
        self.assertEqual(result["balance"], "1.00")
        self.assertEqual(result["currency"], "USD")  

class TestPayment(APITestCase):
    url = "/api/payments/"  
    def setUp(self):
        mock_from_account =Account.objects.create(id='nikki001',balance=100.00,currency = 'USD')
        mock_to_account=Account.objects.create(id='ziko002',balance=1.00,currency = 'USD')
        mock_send_paymend = SendPayment.objects.create(account = mock_from_account, amount=15.00,direction = "outgoing", to_account=mock_to_account)
    def test_get(self):
        #process
        response = self.client.get(self.url)  
        result = response.json()
        #assert
        self.assertEqual(response.status_code, 200) 
        self.assertIsInstance(result, dict)
        self.assertEqual(result[" SEND_PAYMENTS "][0]["account"], "nikki001")
        self.assertEqual(result[" SEND_PAYMENTS "][0]["to_account"], "ziko002")
        self.assertEqual(result[" SEND_PAYMENTS "][0]["amount"], "15.00")
        self.assertEqual(result[" SEND_PAYMENTS "][0]["direction"], "outgoing") 
    def test_post(self):
        # definition
        data = {"account": "nikki001","amount": "5.50", "direction": "outgoing", "to_account": "ziko002"} 
         #process
        response = self.client.post(self.url, data=data)
        result = response.json()
        # assert
        self.assertEqual(response.status_code, 201)
        self.assertEqual(result["amount"], "5.50")
        self.assertEqual(result["direction"], "outgoing")
        self.assertEqual(result["account"], "nikki001") 
        self.assertEqual(result["to_account"], "ziko002") 
        
