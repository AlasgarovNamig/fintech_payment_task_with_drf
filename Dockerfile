FROM python:3.8
ENV PYTHONUNBUFFERED=1
RUN pip install --upgrade pip
RUN mkdir /code 
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY ./core /code/ 
CMD  ./manage.py makemigrations &&  ./manage.py migrate && ./manage.py test && ./manage.py runserver 0.0.0.0:8000
